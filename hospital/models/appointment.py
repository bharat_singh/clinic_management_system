"""This is Hospital|Appointment Model."""
from odoo import models, fields, api
from datetime import datetime


class Appointment(models.Model):
    """There goes the Hospital|Appointment Model."""

    _name = 'hospital.appointment'
    _rec_name = 'appointment_no'
    _description = 'Appointment Information'

    appointment_no = fields.Char(
        'Appointment ID',
        readonly=True
    )

    # Many 2 One | Many Appointments : One Doctor

    doctor = fields.Many2one(
        'hospital.doctor',  # instance of Co-Model | Doctor
        "Doctor"            # string to be displayed
    )

    # Many 2 One | Many Appointments : One Patient

    patient = fields.Many2one(
        'hospital.patient',     # instance of Co-Model | Patient
        "Patient"
    )

    # Selection Field

    state = fields.Selection(
        [('draft', "Draft"),
         ('submit', "Submitted"),
         ('confirm', "Confirmed")],
        "State",
        default='draft'
    )

    # Default Field-Value

    appointment_on = fields.Date('Appointment Date', default=datetime.today())

    # Button & Functions
    @api.one
    def action_submit(self):
        """Submit your appointment application."""
        ref = self.env['ir.sequence'].next_by_code('hospital.appointment') or "NONAME"
        self.write({'state': 'submit', 'appointment_no': ref})
        return True

    @api.one
    def action_confirm(self):
        """Confirm your appointment application."""
        self.write({'state': 'confirm'})
        return True
