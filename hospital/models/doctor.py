"""This is Hospital|Doctor Model."""
from odoo import models, fields, api


class Doctor(models.Model):
    """There goes the Hospital|Doctor Model."""

    _name = 'hospital.doctor'
    _rec_name = 'doctor_name'
    _description = 'Doctor Information'

    doctor_name = fields.Char('Doctor Name', required=True)
    contact_no = fields.Char('Mobile No', size=10)
    experience = fields.Integer('Experience')

    # One 2 Many | One Doctor : Many Patients
    '''
    patients = fields.One2many(
        'hospital.patient',     # instance of Co-Model | Patient
        'doctor',               # Many2One field from Co-Model | Patient
        "Patients"              # string to be displayed
    )
    '''

    # Many 2 Many | Many Doctor : Many Patients

    patients = fields.Many2many(
        'hospital.patient',         # instance of Co-Model | Patient
        'rel_patient_doctor',       # a Many2Many (new)relation name
        'doctor_id', 'patient_id',  # attribute definition for (new)relation
        "Patients"                  # string to be displayed
    )

    # Functional Field

    patient_count = fields.Integer(compute='get_patient_count')

    is_avail = fields.Boolean(
        'Is Available',
        default=True,
        readonly=True
    )

    @api.one
    @api.depends('patients')
    def get_patient_count(self):
        """Compute the Patients appointed to a Doctor."""
        count = 0
        for patient in self.patients:
            count += 1
            self.patient_count = count
        return
