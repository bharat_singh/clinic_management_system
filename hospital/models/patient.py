"""This is Hospital|Patient Model."""
from odoo import models, fields, api
from datetime import datetime


class Patient(models.Model):
    """There goes the Hospital|Patient Model."""

    _name = 'hospital.patient'
    _rec_name = 'patient_name'
    _description = 'Patient Information'

    patient_name = fields.Char('Patient Name', required=True)
    gender = fields.Selection(
        [('male', "Male"),
         ('female', "Female")],
        "Gender"
    )
    dob = fields.Date('Date of Birth')
    age = fields.Integer('Age', compute='get_patient_age')
    admit_since = fields.Date('Admit Date')
    admit_till = fields.Date('Discharge Date')

    # Many 2 One | Many Patients : One Doctor
    '''
    doctor = fields.Many2one(
        'hospital.doctor',  # instance of Co-Model | Doctor
        "Doctor"            # string to be displayed
    )
    '''

    # Many 2 Many | Many Patients : Many Doctor

    doctors = fields.Many2many(
        'hospital.doctor',          # instance from Co-Model | Doctor
        'rel_patient_doctor',       # a Many2Many (new)relation name
        'patient_id', 'doctor_id',  # attribute definition for (new)relation
        "Doctors"                   # string to be displayed
    )

    # Related Fields

    doctor_contact = fields.Char(
        "Doctor's Contact",             # string to be displayed
        related='doctors.contact_no'    # doctors--> Patient:M2M field
                                        # contact_no--> Doctor:contact_no
    )

    @api.one
    @api.depends('dob')
    def get_patient_age(self):
        """Compute the age of the patient."""
        if self.dob:
            self.age = (datetime.now() - datetime.strptime(
                self.dob, '%Y-%m-%d')).days / 365            # : | NOTE
            self.write({'age': self.age})
        return
