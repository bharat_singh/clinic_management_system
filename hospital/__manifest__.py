# iNfo @ Z-Hospitalo | Module f02.05.017-21:25.
# Details given here are shown @Module_Template.
{
    'name': 'Hospital',
    'version': '2.6.3',
    'author': 'Bharat Kumar Singh',
    'description': 'This module is for college project Hospital.',
    'summary': '''This module provides information of doctors, patients and the
     hospital itself.''',
    'category': 'College Project Module',
    'sequence': 5,
    'depends': ['base'],
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/hospital_view.xml',
        'views/doctor_view.xml',
        'views/patient_view.xml',
        'views/appointment_view.xml',
        'data/appointment_seq.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
